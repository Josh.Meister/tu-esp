%%
npv0N = -2.104550213849577e+05;
npv0p1P = -2.102471861339699e+05;
relChange0 = -(npv0p1P-npv0N)/npv0N*100;
npv100p1P= -1.085591017074787e+05;
npv100N = -1.096928916040733e+05;
relChange100 = -(npv100p1P-npv100N)/npv100N*100;
npv40p1P = -1.668724474015657e+05;
npv40N = -1.676191911822837e+05;
relChange40 = -(npv40p1P-npv40N)/npv40N*100;
npv60p1P = -1.467707230833309e+05;
npv60N = -1.476494431111658e+05;
relChange60 = -(npv60p1P-npv60N)/npv60N*100;
%npv0m1P = npv0N-abs(npv0p1P - npv0N);
%%
x = [-1, 0, 1];
plot(x, [-relChange0, 0, relChange0], 'LineWidth', 1.5);
hold on;
plot(x, [-relChange40, 0, relChange40], 'LineWidth', 1.5);
plot(x, [-relChange60, 0, relChange60], 'LineWidth', 1.5);
plot(x, [-relChange100, 0, relChange100], 'LineWidth', 1.5);
yline(0, 'black');
xticks([-1, 0, 1]);
xlabel('Relative Change in Battery Roundtrip Efficiency [%]')
ylabel('Relative Change in NPV_{Spec} [%]')
xlim([-1.05,1.05]);
legend(["CO_{2} Price=0€/t", "CO_{2} Price=78€/t", "CO_{2} Price=118€/t", "CO_{2} Price=198€/t"], 'Location', 'northwest');
