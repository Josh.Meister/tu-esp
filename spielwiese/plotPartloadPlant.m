%%
[prices, productions, co2Prices, marginalTypes] = getPrices();
x = 1:200:length(prices);
%% 
sP = zeros(size(prices));
sMT = zeros(size(marginalTypes));
for i = 1:length(prices(:,1))
    [sP(i,:) I] = sort(prices(i,:), 'descend');
    sMT(i,:) = marginalTypes(i,I);
end
%%
my = [0,0.4470,0.7410;0.8500,0.3250,0.0980;0.9290,0.6940,0.1250;0.4940,0.1840,0.5560;0.4660,0.6740,0.1880;0.3010,0.7450,0.9330];%0.6350,0.0780,0.1840];
colormap(my);
c = imagesc(x, co2Prices, sMT(:,x));%sortedMT(:,x));
col = colorbar('Ticks', [0,1,2,3,4,5], 'TickLabels',{'No Plant running','Lignite', 'HardCoal', 'Steam turbine', 'Open Cycle', 'Combined Cycle'});
xlabel("Hours of the Life Cycle sorted by Price");
ylabel("CO_{2} Price [€/t]");
%);
hold on;
set(gca,'YDir','normal');
