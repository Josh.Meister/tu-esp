sP = getMeritOrder(0);
capacity = zeros(length(sP(:,1)));
c = 0;
for i=1:length(sP(:,1))
    c = c+sP(i,2);
    capacity(i) = c;
end
costs = sP(:,7)+sP(:,8);
x = linspace(0,7.4e4, 4000);
prices2 = electricityPrice(x, 0);

plot(capacity, costs);
hold on;
plot(x, prices2, 'Color', [1 0 0]);%, 'LineWidth', 1.1);
xlabel("Demand [MW]");
ylabel("Electricity Price [€/MWh]");
legend(["Static Merit Order"], ["Merit Order including Partload Plants"])
