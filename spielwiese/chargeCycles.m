%% 
[prices productions co2Prices] = getPrices();
%%
p = prices(1,:);
%%
battery.energy = 80;
battery.power = 20;
battery.roundTrip = 0.9;
FCE = @(cc) sum(abs(cc(1,:)))/battery.energy;
%%
powers = linspace(1,100,50);
l = 1000;
ccs = [];%zeros(length(powers), l); 

for i = 1:length(powers)
    battery.power = powers(i);
    cc = (calculateChargeCycles(p, battery, 0, 4));
    ccs(i,:) = cc(5,1:round(end/l):end);
end
%% 
