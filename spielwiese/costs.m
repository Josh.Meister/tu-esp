%%
costs = [66, 200, 25, 33];
powers = [100, 300, 25, 30];
capacities = [129, 1200, 50, 30];
%% 
ccosts = powers ./ capacities;
scatter(capacities, ccosts*10^3, 'filled');
xlabel("Capacity in MWh")
ylabel("Spec Price in " + string(char(8364)) + "/kWh")
%%
pcosts = costs ./ powers;
scatter(powers, pcosts*10^3, 'filled');
xlabel("Power in MW");
ylabel("Spec Price in " + string(char(8364)) + "/MW");
%%
[prices, productions, co2Prices] = getPrices();
battery.energy = 70;
battery.power = battery.energy/4;
battery.roundTrip = 0.9;
soc = @(cc) mean(cc(3,:)/battery.energy);
%%
hold on;
for i = linspace(30,100,50)
    scatter(i, soc(calculateChargeCycles(prices(1,:),battery,0,i)),'filled');
end
xlabel("Sell over [Euros]");
ylabel('SoC [%]')
%%
hold on;
for i = linspace(30,100,50)
    scatter(i, soc(calculateChargeCycles(prices(1,:),battery,0,i)),'filled');
end
xlabel("Sell over [Euros]");
ylabel('SoC [%]')
%%
hold on;
for i = 1:length(co2Prices)
    l = length(prices(i, prices(i,:) >= 30));
    scatter(co2Prices(i), l,'filled');
end
xlabel("Co2Price");
ylabel("Hours with Price over 30 Euros")
%%
mO1 = getMeritOrder(18);
mO2 = getMeritOrder(24);
pr1 = mO1(:,7) + mO1(:,8);
pr2 = mO2(:,7) + mO2(:,8);
hold on;
plot(pr1);
plot(pr2);
yline(30,"g",'LineWidth', 2);
legend("before the jump (18Euros/ton)", "after the jump (24Euros/ton)","sellover boundry");
xlabel("Power");
ylabel("Merit order specific price");
