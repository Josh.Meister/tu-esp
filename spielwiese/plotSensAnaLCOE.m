%%
rC = @(lc1, lc2) (lc2-lc1)/lc1 * 100;
lcoe0p = 2.162055711129790e+02;
lcoe0N = 2.176869804317517e+02;
relChange0 = rC(lcoe0N,lcoe0p);
lcoe40p = 2.150998915850066e+02;
lcoe40N = 2.149422768903675e+02
relChange40 = rC(lcoe40N, lcoe40p);
lcoe60p = 2.138136707859417e+02;
lcoe60N = 2.151539141552574e+02;
relChange60 = rC(lcoe60N, lcoe60p);
lcoe100p = 2.136823979396594e+02;
lcoe100N = 2.150389646268302e+02;
relChange100 = rC(lcoe100N, lcoe100p);
%%
x = [-1, 0, 1];
plot(x, [-relChange0, 0, relChange0], 'LineWidth', 1.5);
hold on;
plot(x, [-relChange40, 0, relChange40], 'LineWidth', 1.5);
plot(x, [-relChange60, 0, relChange60], 'LineWidth', 1.5);
plot(x, [-relChange100, 0, relChange100], 'LineWidth', 1.5);
yline(0, 'black');
xticks([-1, 0, 1]);
xlabel('Relative Change in Battery Roundtrip Efficiency [%]')
ylabel('Relative Change in LCOE [%]')
xlim([-1.05,1.05]);
legend(["CO_{2} Price=0€/t", "CO_{2} Price=78€/t", "CO_{2} Price=118€/t", "CO_{2} Price=198€/t"], 'Location', 'northeast');
