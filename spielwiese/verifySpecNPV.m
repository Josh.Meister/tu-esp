%{
caps = linspace(1,100,100);
prices = getPrices();
%npvs = zeros(length(caps));
npvs = [];
%battery.power = 25;
%battery.energy = 1;
battery.roundTrip = 0.85;
s = scenarios();
s = s(1, :);

for i = 1:length(caps)
    disp(i);
    battery.energy = caps(i);
    battery.power = caps(i)*0.25;
    cc = calculateChargeCycles(prices(1,:), battery, 0, 40);
    lCC = calculateLifeCycleCosts(battery, s, cc);
    npv = calculateNPV(cc, lCC);
    npvs(i) = npv;
end
%}
plot(caps, npvs, "x", 'MarkerSize', 5);
xlabel("Storage Capacity [MWh]")
ylabel("NPV");
