battery.energy = 1;
battery.roundTrip = 0.85;
prices = getPrices();

powers = [0.1, 0.25, 1];
t = linspace(0,20, length(prices(1,:)));
tS = linspace(0,20, 10);
lArray = [];
l = lines();
hold on;
markerArray = ["s", "d", "<", ">"];
for i=1:length(powers)
    battery.power = powers(i);
    cc = calculateChargeCycles(prices(1,:), battery, 0, 0);
    yyaxis left;
    plot(t, cc(5,:), "-",'Color', 'b', 'HandleVisibility', 'off');
    scatter(t(1:1752*10:end), cc(5, 1:1752*10:end), markerArray(i), 'Filled', 'MarkerEdgeColor', 'black');
    lArray = [lArray, 'P/S=' + string(powers(i))];

    yyaxis right;
    plot(t, cc(6,:), "-",'Color', 'r', 'HandleVisibility', 'off');
    scatter(t(1:1752*10:end), cc(6, 1:1752*10:end), markerArray(i), 'Filled', 'MarkerEdgeColor', 'black');
end
yyaxis left;
ax = gca();
legend(lArray, 'Location', 'north');
yline(0.8, 'HandleVisibility', 'off');
xlabel("Time [a]");
ylabel("Relative Capacity [1]");
ylim([0.75, 1]);
yyaxis right;
ylabel("Equivalent Full Cycles [1]");

yyaxis left;

box on;
