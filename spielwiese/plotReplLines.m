load("data/fullNPVGrid.mat");

npvs = squeeze(npvs(:,:,:,1));
nSizes = sizes(1:end-1);
nSellOvers = sellOvers(1:end-1);

hold on;
lArray = [];
for i = [100  sort(1:15:100, 'descend')]
    xP = [];
    yP = [];
    npv = squeeze(npvs(i,:,:));
    diffx = diff(npv);
    diffy = diff(npv');
    diffy = diffy';
    posX = (abs(diffx) > 1e5);
    posY = (abs(diffy) > 1e5);
    posX = posX(:,1:end-1);
    posY = posY(1:end-1, :);
    lPos = posX+posY;
    for m = 1:149
        for n = 1:199
            if lPos(m,n) ~= 0
                xP = [xP, nSellOvers(m)];
                yP = [yP, nSizes(n)];
            end
        end
    end
    area(yP, xP);
    lArray = [lArray, "CO_{2}Price: " + string(co2Prices(i)) + " €/t"];
end
legend(lArray, 'Location', 'southeast');
xlabel("S/P [W/Wh]");
ylabel("SOP [€]");
xlim([0,1]);
ylim([0,150]);
box on;

%c = imagesc(nSizes, nSellOvers, lPos);
%set(gca,'YDir','normal');
