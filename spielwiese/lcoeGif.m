%% Load the grids
grids = load("data/fullLCOEGrid.mat");
lcoes = grids.lcoes;
sizes = grids.sizes;
sellOvers = grids.sellOvers;
%% plot one with max
%{
n = 99;
upLim = 1e3;
lcoeGrid = squeeze(lcoes(n, :, :, 1));
colormap jet;
surface(sizes, sellOvers, lcoeGrid, 'EdgeAlpha', 0.5);
hold on;
[zp,xi,yi] = findMapMin(lcoeGrid);
scatter3(sizes(yi), sellOvers(xi), zp, 90, 'filled', 'MarkerFaceColor', 'red');
plot3([sizes(yi), sizes(yi)], [sellOvers(xi), sellOvers(xi)], [0, upLim], 'LineWidth', 3);
xlabel("sizes [kWh/kW]")
ylabel("sellovers [€]")
zlabel("lcoe [€/MWh]")
view(05, 40);
legend("co2Price: " + string(n*2 - 2));
zlim([0, upLim]);
caxis([0, upLim]);
%}
%% make da gif
h = figure;
axis tight manual % this ensures that getframe() returns a consistent size
filename = 'lcoeGrid.gif';
for n = 1:100
    % plot it
    upLim = 6.5e2;
    lcoeGrid = squeeze(lcoes(n, :, :, 1));
    colormap jet;
    set(gcf,'Position',[0 0 1280 720]);
    mesh(sizes, sellOvers, lcoeGrid);
    hold on;
    [zp,xi,yi] = findMapMin(lcoeGrid);
    scatter3(sizes(yi), sellOvers(xi), zp, 90, 'filled', 'MarkerFaceColor', 'red');
    plot3([sizes(yi), sizes(yi)], [sellOvers(xi), sellOvers(xi)], [0, upLim], 'LineWidth', 3);
    xlabel("P/S [W/Wh]");
    ylabel("SOP [€]");
    zlabel("LCOE [€/MWh]");
    view(-65, 64);
    legend("CO_{2} Price: " + string(n*2 - 2) + " €/t", 'Location', 'southwest');
    xlim([0.06,1]);
    ylim([0,125]);
    zlim([0, upLim]);
    caxis([0, upLim]);
    drawnow;
    %zlim([])
      % Capture the plot as an image 
    frame = getframe(h); 
    im = frame2im(frame); 
    [imind,cm] = rgb2ind(im,256); 
    % Write to the GIF File 
    if n == 1 
      imwrite(imind,cm,filename,'gif', 'Loopcount',inf); 
    else 
      imwrite(imind,cm,filename,'gif','WriteMode','append'); 
    end 
    clf;
  end
