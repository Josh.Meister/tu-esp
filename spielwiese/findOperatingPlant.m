function plant = findOperatingPlant(production, co2Price, partloadFits)
    sP = getMeritOrder(co2Price);
    for i = 1:length(sP)
        cap = sum(sP(1:i, 2));
        if cap + sP(i+1, 2) > production
            break
        end
    end
    avPlants = sP(i+1:end,:);
    gap = production - cap;
    plant = findPartloadPlant(avPlants, gap, partloadFits);
    if ~isnan(plant)
        return
    elseif sP(1, 2) > production
        plant = zeros(size(sP(1,:)));
        return
    else
        plant = sP(i, :);
    return
end
