%% Load the grids
grids = load("data/fullNPVGrid.mat");
noReplGrids = load("data/noReplNPVGrid.mat");
npvs = grids.npvs;
noRNpvs = noReplGrids.npvs;
noRSizes = noReplGrids.sizes;
noRSellOvers = noReplGrids.sellOvers;
sizes = grids.sizes;
sellOvers = grids.sellOvers;
%% plot one with max
%{
n = 80;
npvGrid = squeeze(npvs(n, :, :, 1));
colormap jet;
mesh(sizes, sellOvers, npvGrid);
hold on;
[zp,xi,yi] = findMapMax(npvGrid);
scatter3(sizes(yi), sellOvers(xi), zp, 90, 'filled');
set(gcf,'Position',[0 0 1280 720])
xlabel("sizes [kWh/kW]")
ylabel("sellovers [€]")
zlabel("NPV [€]")
view(25, 40);
legend("co2Price: " + string(n*2 - 2), 'Location', 'southwest');
zlim([-7e5, -1e5]);
%}
%% make da gif
h = figure;
upLim = -1e5;
axis tight manual % this ensures that getframe() returns a consistent size
filename = 'npvGrids2.gif';
for n = 1:100
    % plot it
    npvGrid = squeeze(npvs(n, :, :, 1));
    npvNoRepl = squeeze(noRNpvs(n, :, :, 1));
    colormap jet;
    mesh(sizes, sellOvers, npvGrid);
    set(gcf,'Position',[0 0 1280 720]);
    hold on;
    [zp,xi,yi] = findMapMax(npvGrid);
    [zpNR, xiNR, yiNR] = findMapMax(npvNoRepl);
    scatter3(sizes(yi), sellOvers(xi), zp, 90, 'filled');
    plot3([sizes(yi), sizes(yi)], [sellOvers(xi), sellOvers(xi)], [-7e5, upLim], 'LineWidth', 3, 'Color', 'blue');

    scatter3(noRSizes(yiNR), noRSellOvers(xiNR), zpNR, 90, 'filled');
    plot3([noRSizes(yiNR), noRSizes(yiNR)], [noRSellOvers(xiNR), noRSellOvers(xiNR)], [-7e5, upLim], 'LineWidth', 3, 'Color', 'green');
    xlabel('P/S [W/Wh]')
    ylabel('SOP [€]')
    zlabel('NPV_{Spec} [€/MWh]')
    view(25, 40);
    zlim([-7e5, -1e5]);
    legend("CO_{2} Price: " + string(n*2 - 2) + " €/t", 'Location', 'southwest');
    drawnow;
    %zlim([])
      % Capture the plot as an image 
    frame = getframe(h); 
    im = frame2im(frame); 
    [imind,cm] = rgb2ind(im,256); 
    % Write to the GIF File 
    if n == 1 
      imwrite(imind,cm,filename,'gif', 'Loopcount',inf); 
    else 
      imwrite(imind,cm,filename,'gif','WriteMode','append'); 
    end 
    clf;
  end
