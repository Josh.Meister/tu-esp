function [prices, productions, co2Prices, marginalTypes] = getPrices()
    finalMarketCurves = load("data/finalMarketCurves.mat");
    prices = finalMarketCurves.prices;
    productions = finalMarketCurves.productions;
    co2Prices = finalMarketCurves.co2Prices;
    marginalTypes = finalMarketCurves.marginalTypes;
end
