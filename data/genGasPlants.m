%%
noCHP = readtable('power_plant_list_Low_coal_complete.xlsx','Sheet','Natural_Gas_CHP '); 
olddates = noCHP.AufnahmeDerKommerziellenStromerzeugungDerDerzeitInBetriebBefind;
dates = [datetime(1,1,1)];
for i = 1:length(olddates)
    try
        dates(i) = datetime(olddates(i));
    catch ME
        try
            dates(i) = datetime(olddates(i), 'InputFormat', 'yyyy');
        catch ME
            dates(i) = "";
        end
    end
end

noCHP = table(string(noCHP.KraftwerksnummerBundesnetzagentur), dates', noCHP.Netto_Nennleistung_elektrischeWirkleistung_InMW, noCHP.efficiency, noCHP.Var24, noCHP.Var25);
noCHP.Properties.VariableNames = {'Id','Date','Capacity','Efficiency','Technology','TechNumbers'};

CHP = readtable('power_plant_list_Low_coal_complete.xlsx','Sheet','Natural_Gas_NoCHP'); 
olddates = CHP.AufnahmeDerKommerziellenStromerzeugungDerDerzeitInBetriebBefind;
dates = [datetime(1,1,1)];
for i = 1:length(olddates)
    try
        dates(i) = datetime(olddates(i));
    catch ME
        try
            dates(i) = datetime(olddates(i), 'InputFormat', 'yyyy');
        catch ME
            dates(i) = "";
        end
    end
end

CHP = table(string(CHP.KraftwerksnummerBundesnetzagentur), dates', CHP.Netto_Nennleistung_elektrischeWirkleistung_InMW, CHP.efficiency, CHP.Var24, CHP.Var25);
CHP.Properties.VariableNames = {'Id','Date','Capacity','Efficiency','Technology','TechNumbers'};

hardCoal = readtable('power_plant_list_Low_coal_complete.xlsx','Sheet','Bituminous '); 
olddates = hardCoal.AufnahmeDerKommerziellenStromerzeugungDerDerzeitInBetriebBefind;
dates = [datetime(1,1,1)];
for i = 1:length(olddates) try dates(i) = datetime(olddates(i));
    catch ME
        try
            dates(i) = datetime(olddates(i), 'InputFormat', 'yyyy');
        catch ME
            dates(i) = "";
        end
    end
end

hardCoal = table(string(hardCoal.KraftwerksnummerBundesnetzagentur), dates', hardCoal.Netto_Nennleistung_elektrischeWirkleistung_InMW, hardCoal.efficiency, hardCoal.Var24,hardCoal.Var25);
hardCoal.Properties.VariableNames = {'Id','Date','Capacity','Efficiency','Technology','TechNumbers'};

lignite = readtable('power_plant_list_Low_coal_complete.xlsx','Sheet','Lignite'); 
olddates = lignite.AufnahmeDerKommerziellenStromerzeugungDerDerzeitInBetriebBefind;
dates = [datetime(1,1,1)];
for i = 1:length(olddates)
    try
        dates(i) = datetime(olddates(i));
    catch ME
        try
            dates(i) = datetime(olddates(i), 'InputFormat', 'yyyy');
        catch ME
            dates(i) = "";
        end
    end
end

lignite = table(string(lignite.KraftwerksnummerBundesnetzagentur), dates', lignite.Netto_Nennleistung_elektrischeWirkleistung_InMW, lignite.efficiency, lignite.Var24, lignite.Var25);
lignite.Properties.VariableNames = {'Id','Date','Capacity','Efficiency','Technology','TechNumbers'};

%% Gen Plants
linFit = @(x,x1,x2,y1,y2) y1 + (y2-y1)/(x2-x1)*(x-x1);
gtEff = @(x) linFit(x, 2010, 2020, 40.6/100,43.2/100);
ccgtEff = @(x) linFit(x, 2010, 2020, 58.5/100, 63.0/100);

% effs: 65% combined cycles, 45% open cycle

techs = [""];
technumbers = [];
effs = [];
capacities = [];
names = [""];
matimes = [datetime(1,1,1)];

times = linspace(2020, 2030, 25);
for i=1:length(times)
    i1 = 2*i-1;
    i2 = 2*i;
    %gt
    techs(i1) = "Gas turbine";
    technumbers(i1) = 6;
    effs(i1) = gtEff(times(i));
    capacities(i1) = 700;
    names(i1) = "Gen" + i1;
    matimes(i1) = datetime(floor(times(i)),round(mod(times(i),1)*12),1);
    %ccgt
    techs(i2) = "Combined cycle";
    technumbers(i2) = 7;
    effs(i2) = ccgtEff(times(i));
    capacities(i2) = 700;
    names(i2) = "Gen" + i2;
    matimes(i2) = datetime(floor(times(i)),round(mod(times(i),1)*12),1);
end

genPlants = table(names', matimes', capacities', effs', techs', technumbers');
genPlants.Properties.VariableNames = {'Id','Date','Capacity','Efficiency','Technology','TechNumbers'};

%% Write to matrix
%lignite
plantData = [];
plantNames = ["",""];
plantTypes = ["5", "steam turbine";"6", "gas turbine"; "7", "combined cycle"; "8", "combustion engine"];
% data: IdNumber, Capacity, Efficiency, Technology, Fueltype, isCHP
for i=1:length(hardCoal.Id)
    plantData(i,:) = [i, hardCoal.Capacity(i), hardCoal.Efficiency(i), hardCoal.TechNumbers(i), 0, 0];
    plantNames(i,:) = [string(i), hardCoal.Id(i)];
end
offset = length(plantData(:,1));
for i=1:length(lignite.Id)
    plantData(i+offset,:) = [i+offset, lignite.Capacity(i), lignite.Efficiency(i), lignite.TechNumbers(i), 1, 0];
    plantNames(i+offset,:) = [string(i+offset), lignite.Id(i)];
end
offset = length(plantData(:,1));
for i=1:length(noCHP.Id)
    plantData(i+offset,:) = [i+offset, noCHP.Capacity(i), noCHP.Efficiency(i), noCHP.TechNumbers(i), 2, 0];
    plantNames(i+offset,:) = [string(i+offset), noCHP.Id(i)];
end
offset = length(plantData(:,1));
for i=1:length(CHP.Id)
    plantData(i+offset,:) = [i+offset, CHP.Capacity(i), CHP.Efficiency(i), CHP.TechNumbers(i), 2, 1];
    plantNames(i+offset,:) = [string(i+offset), CHP.Id(i)];
end
%% Write Gen Data too
offset = length(plantData(:,1));
for i=1:length(genPlants.Id)
    plantData(i+offset,:) = [i+offset, genPlants.Capacity(i), genPlants.Efficiency(i), genPlants.TechNumbers(i), 2, 0];
    plantNames(i+offset,:) = [string(i+offset), genPlants.Id(i)];
end
%fill up the NaN
for i=1:length(plantData(:,4))
    if isnan(plantData(i,4))
        plantData(i,4)=5;
    end
end
