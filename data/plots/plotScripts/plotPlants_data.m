%%
% data from "useful link"
data = readtable('conventional_power_plants_DE.xlsx');
opsData = table(string(data.id), string(data.fuel), string(data.technology), string(data.chp), data.efficiency_estimate);
opsData.Properties.VariableNames = {'Id','Fuel','Technology','CHP','Efficiency_estimate'};
%%
% le data processing
CHP = readtable('power_plant_list_Low_coal.xlsx','Sheet','Natural_Gas_CHP '); 

dates = CHP.AufnahmeDerKommerziellenStromerzeugungDerDerzeitInBetriebBefind;
newDates = [datetime(1999,1,1)];
for i=1:length(dates)
    try
        newDates(i) = datetime(dates(i));
    catch ME
        try
            newDates(i) = datetime(dates(i), 'InputFormat', 'yyyy');
        catch ME
            newDates(i) = NaT;
            disp(dates(i));
        end
    end
end
bnaId = string(CHP.KraftwerksnummerBundesnetzagentur);
technologies = [""];
fuels = [""];
for i = 1:length(bnaId)
    try
        technologies(i) = opsData.Technology(find(ismember(opsData.Id,bnaId(i))));
        fuels(i) = opsData.Fuel(find(ismember(opsData.Id,bnaId(i))));
    catch ME
        technologies(i) = "unknown";
        fuels(i) = "";
    end
end
CHP = table(string(CHP.KraftwerksnummerBundesnetzagentur),newDates',CHP.Netto_Nennleistung_elektrischeWirkleistung_InMW, CHP.efficiency,technologies',fuels');
CHP.Properties.VariableNames = {'Id','Date', 'Capacity', 'Efficiency','Technology','Fuel'};

% noCHP Data
noCHP = readtable('power_plant_list_Low_coal.xlsx','Sheet','Natural_Gas_NoCHP');

dates = noCHP.AufnahmeDerKommerziellenStromerzeugungDerDerzeitInBetriebBefind
newDates = [datetime(1999,1,1)]
for i=1:length(dates)
    try
        newDates(i) = datetime(dates(i));
    catch ME
        try
            newDates(i) = datetime(dates(i), 'InputFormat', 'yyyy');
        catch ME
            newDates(i) = NaT;
            disp(dates(i));
        end
    end
end
bnaId = string(noCHP.KraftwerksnummerBundesnetzagentur);
technologies = [""];
fuels = [""];
for i = 1:length(bnaId)
    try
        technologies(i) = opsData.Technology(find(ismember(opsData.Id,bnaId(i))));
        fuels(i) = opsData.Fuel(find(ismember(opsData.Id,bnaId(i))));
    catch ME
        technologies(i) = "unknown";
        fuels(i) = "";
    end
end
noCHP = table(string(noCHP.KraftwerksnummerBundesnetzagentur),newDates',noCHP.Netto_Nennleistung_elektrischeWirkleistung_InMW, noCHP.efficiency,technologies',fuels');
noCHP.Properties.VariableNames = {'Id','Date', 'Capacity', 'Efficiency','Technology','Fuel'};

 % hard coal Data
hardCoal = readtable('power_plant_list_Low_coal.xlsx','Sheet','Bituminous '); 

dates = hardCoal.AufnahmeDerKommerziellenStromerzeugungDerDerzeitInBetriebBefind;
newDates = [datetime(1999,1,1)];
for i=1:length(dates)
    try
        newDates(i) = datetime(dates(i));
    catch ME
        try
            newDates(i) = datetime(dates(i), 'InputFormat', 'yyyy');
        catch ME
            newDates(i) = NaT;
            disp(dates(i));
        end
    end
end
bnaId = string(hardCoal.KraftwerksnummerBundesnetzagentur);
technologies = [""];
fuels = [""];
for i = 1:length(bnaId)
    try
        technologies(i) = opsData.Technology(find(ismember(opsData.Id,bnaId(i))));
        fuels(i) = opsData.Fuel(find(ismember(opsData.Id,bnaId(i))));
    catch ME
        technologies(i) = "unknown";
        fuels(i) = "";
    end
end
hardCoal = table(string(hardCoal.KraftwerksnummerBundesnetzagentur),newDates',hardCoal.Netto_Nennleistung_elektrischeWirkleistung_InMW, hardCoal.efficiency,technologies',fuels');
hardCoal.Properties.VariableNames = {'Id','Date', 'Capacity', 'Efficiency','Technology','Fuel'};
% lignite
lignite = readtable('power_plant_list_Low_coal.xlsx','Sheet','Lignite'); 

dates = lignite.AufnahmeDerKommerziellenStromerzeugungDerDerzeitInBetriebBefind;
newDates = [datetime(1999,1,1)];
for i=1:length(dates)
    try
        newDates(i) = datetime(dates(i));
    catch ME
        try
            newDates(i) = datetime(dates(i), 'InputFormat', 'yyyy');
        catch ME
            newDates(i) = NaT;
            disp(dates(i));
        end
    end
end
bnaId = string(lignite.KraftwerksnummerBundesnetzagentur);
technologies = [""];
fuels = [""];
for i = 1:length(bnaId)
    try
        technologies(i) = opsData.Technology(find(ismember(opsData.Id,bnaId(i))));
        fuels(i) = opsData.Fuel(find(ismember(opsData.Id,bnaId(i))));
    catch ME
        technologies(i) = "unknown";
        fuels(i) = "";
    end
end
lignite = table(string(lignite.KraftwerksnummerBundesnetzagentur),newDates',lignite.Netto_Nennleistung_elektrischeWirkleistung_InMW, lignite.efficiency,technologies',fuels');
lignite.Properties.VariableNames = {'Id','Date', 'Capacity', 'Efficiency','Technology','Fuel'};
%% Fill in the unknowns
CHP((CHP.Technology == "unknown" & CHP.Efficiency < 0.45),:).Technology(:) = "Steam turbine";
CHP((CHP.Technology == "unknown" & CHP.Efficiency > 0.45),:).Technology(:) = "Combined cycle";
%%
[o, I] = sort(CHP.Date, 'descend');
sortedCHP = table(CHP.Date(I), CHP.Capacity(I), CHP.Efficiency(I));
[o, I] = sort(noCHP.Date, 'descend');
sortedNoCHP = table(noCHP.Date(I), noCHP.Capacity(I), noCHP.Efficiency(I));
[o, I] = sort(hardCoal.Date, 'descend');
sortedHardCoal = table(hardCoal.Date(I), hardCoal.Capacity(I), hardCoal.Efficiency(I));
[o, I] = sort(lignite.Date, 'descend');
sortedHardCoal = table(lignite.Date(I), lignite.Capacity(I), lignite.Efficiency(I));
%%
technologies = ["Steam turbine", "Gas turbine", "Combined cycle", "Combustion engine", "unknown"];
%hold on;
%for i = technologies
%    data = CHP((CHP.Technology == i),:);
%    sc = scatter(data.Date, data.Efficiency,  'filled');
%    sc.DataTipTemplate.DataTipRows(1).Label = 'Name';
%    sc.DataTipTemplate.DataTipRows(1) = dataTipTextRow('Name',data.Id');
%end
%legend(technologies);
  
sc = gscatter(ymd(CHP.Date), CHP.Efficiency, CHP.Technology);%CHP.Capacity, 'filled');
%sc.MarkerFaceAlpha = 0.5;
%sc.DataTipTemplate.DataTipRows(1).Label = 'Name';
%sc.DataTipTemplate.DataTipRows(end + 1) = dataTipTextRow('Name',CHP.Id');
grid();
xlim(ymd([datetime(1946,1,1),datetime(2020,1,1)]));
xlabel("Time of Commissioning [year]");
ylabel("Efficiency [1]");
%legend("CHP-Data by Tobias, radius ~ capacity", 'location', 'northwest');
%%
sc = gscatter(noCHP.Date, noCHP.Efficiency, noCHP.Technology);%, 'filled');
%sc.MarkerFaceAlpha = 0.5;
grid();
xlim([ymd(datetime(1970,1,1)),ymd(datetime(2020,1,1))]);
xlabel("Time of Commissioning [year]");
ylabel("Efficiency [1]");
%legend("noCHP-Data by Tobias, radius ~ capacity", 'location', 'northwest');
%%
sc = gscatter(hardCoal.Date, hardCoal.Efficiency, hardCoal.Technology);
%sc.MarkerFaceAlpha = 0.5;
grid();
xlim([datetime(1988,1,1),datetime(2020,1,1)]);
xlabel("Time of Commissioning [year]");
ylabel("Efficiency [1]");
%legend("hardCoal-Data by Tobias, radius ~ capacity", 'location', 'northwest');
%% 
sc = gscatter(lignite.Date, lignite.Efficiency, lignite.Technology);
%sc.MarkerFaceAlpha = 0.5;
grid();
xlim([datetime(1993,1,1),datetime(2020,1,1)]);
xlabel("Time of Commissioning [year]");
ylabel("Efficiency [1]");
%legend("lignite-Data by Tobias, radius ~ capacity", 'location', 'northwest');
%% write the technologies
filename = 'data/power_plant_list_Low_coal_technologies.xlsx';
writematrix(CHP.Technology,filename,'Sheet','Natural_Gas_CHP ','Range', 'X2'); 
writematrix(noCHP.Technology,filename,'Sheet','Natural_Gas_NoCHP','Range', 'X2');
writematrix(lignite.Technology,filename,'Sheet','Lignite','Range', 'X2'); 
writematrix(hardCoal.Technology,filename,'Sheet','Bituminous ','Range', 'X2'); 
%%
% write technologies as numbers
techNumbers = [1];
for i=1:length(CHP.Technology)
    if CHP.Technology(i)=="Steam turbine"
        techNumbers(i) = 5;
    elseif CHP.Technology(i)=="Gas turbine"
        techNumbers(i) = 6;
    elseif CHP.Technology(i)=="Combined cycle" techNumbers(i) = 7;
    elseif CHP.Technology(i)=="Combustion Engine"
        techNumbers(i) = 8;
    else
        techNumbers(i) = NaN;
    end
end
writematrix(techNumbers',filename,'Sheet','Natural_Gas_CHP ','Range', 'Y2'); 
CHP.TechNumbers = techNumbers';
techNumbers = [1];
for i=1:length(noCHP.Technology)
    if noCHP.Technology(i)=="Steam turbine"
        techNumbers(i) = 5;
    elseif noCHP.Technology(i)=="Gas turbine"
        techNumbers(i) = 6;
    elseif noCHP.Technology(i)=="Combined cycle"
        techNumbers(i) = 7;
    elseif noCHP.Technology(i)=="Combustion Engine"
        techNumbers(i) = 8;
    else
        techNumbers(i) = NaN;
    end
end
writematrix(techNumbers',filename,'Sheet','Natural_Gas_NoCHP','Range', 'Y2');
noCHP.TechNumbers = techNumbers';
techNumbers = [1];
for i=1:length(lignite.Technology)
    if lignite.Technology(i)=="Steam turbine"
        techNumbers(i) = 5;
    elseif lignite.Technology(i)=="Gas turbine"
        techNumbers(i) = 6;
    elseif lignite.Technology(i)=="Combined cycle"
        techNumbers(i) = 7;
    elseif lignite.Technology(i)=="Combustion Engine"
        techNumbers(i) = 8;
    else
        techNumbers(i) = NaN;
    end
end
writematrix(techNumbers',filename,'Sheet','Lignite','Range', 'Y2'); 
lignite.TechNumbers = techNumbers';
techNumbers = [1];
for i=1:length(hardCoal.Technology)
    if hardCoal.Technology(i)=="Steam turbine"
        techNumbers(i) = 5;
    elseif hardCoal.Technology(i)=="Gas turbine"

        techNumbers(i) = 6;
    elseif hardCoal.Technology(i)=="Combined cycle"
        techNumbers(i) = 7;
    elseif hardCoal.Technology(i)=="Combustion Engine"
        techNumbers(i) = 8;
    else
        techNumbers(i) = NaN;
    end
end
writematrix(techNumbers,filename,'Sheet','Bituminous ','Range', 'Y2'); 
hardCoal.TechNumbers = techNumbers';
%% writing everything into matrices for later use
%lignite
plantData = [];
plantNames = ["",""];
plantTypes = ["5", "steam turbine";"6", "gas turbine"; "7", "combined cycle"; "8", "combustion engine"];
% data: IdNumber, Capacity, Efficiency, Technology, Fueltype, isCHP
for i=1:length(hardCoal.Id)
    plantData(i,:) = [i, hardCoal.Capacity(i), hardCoal.Efficiency(i), hardCoal.TechNumbers(i), 0, 0];
    plantNames(i,:) = [string(i), hardCoal.Id(i)];
end
offset = length(plantData(:,1));
for i=1:length(lignite.Id)
    plantData(i+offset,:) = [i+offset, lignite.Capacity(i), lignite.Efficiency(i), lignite.TechNumbers(i), 1, 0];
    plantNames(i+offset,:) = [string(i+offset), lignite.Id(i)];
end
offset = length(plantData(:,1));
for i=1:length(noCHP.Id)
    plantData(i+offset,:) = [i+offset, noCHP.Capacity(i), noCHP.Efficiency(i), noCHP.TechNumbers(i), 2, 0];
    plantNames(i+offset,:) = [string(i+offset), noCHP.Id(i)];
end
offset = length(plantData(:,1));
for i=1:length(CHP.Id)
    plantData(i+offset,:) = [i+offset, CHP.Capacity(i), CHP.Efficiency(i), CHP.TechNumbers(i), 2, 1];
    plantNames(i+offset,:) = [string(i+offset), CHP.Id(i)];
end
%fill up the NaN
for i=1:length(plantData(:,4))
    if isnan(plantData(i,4))
        plantData(i,4)=5;
    end
end
%%

