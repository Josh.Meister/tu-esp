[prices, productions, co2Prices, mPT] = getPrices();
hold on;
means = [];
maxes = [];
medians = [];
for i = 1:length(co2Prices)
    means = [means mean(prices(i,:))];
    maxes = [maxes max(prices(i,:))];
    medians = [medians median(prices(i,:))];
end

plot(co2Prices, means);
plot(co2Prices, maxes);
plot(co2Prices, medians);

legend("Mean", "Max", "Median", 'Location','Northwest');
ylabel("Electricity Price [€/MWh]");
xlabel("CO2 Price [€/t]");
