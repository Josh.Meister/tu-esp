%%
x = load("data/npvs.mat");
sellOvers = x.sellovers;
sizeRatios = x.sizeratios;
npvs = x.npvs;
%% Mesh plot
mesh(sellOvers, sizeRatios, npvs);
colormap jet;
colorbar;
xlabel("Sellover prices [€]");
ylabel("Size Ratios [1]");
zlabel("NPV's for scenario 1, co2Prices 1");

%% Height plot
c = imagesc(sizeRatios, sellOvers, npvs');
set(gca, 'YDir', 'normal');
ylabel("Sellover prices [€]");
xlabel("Size Ratios [1]");
colormap jet;
colorbar;
caxis([-1.4, -0.4]*10^5);
col.Label.String = "NPV's for scenario 1, co2Prices 1";
