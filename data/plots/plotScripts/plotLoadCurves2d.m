%% get data
[prices, productions, co2Prices, mPT] = getPrices();

%sortedPlants = zeros(length(co2Prices), size(getMeritOrder(1)));
sortedPrices = zeros(size(prices));
sortedProductions = zeros(size(productions));
sortedTypes = zeros(size(mPT));
for i = 1:length(prices(:,1))
    [sortedPrices(i,:) I] = sort(prices(i,:), 'descend');
    sortedProductions(i,:) = productions(i,I);
    sortedTypes(i,:) = mPT(i,I);
    %sortedPlants(i,:) = getMeritOrder(co2Prices(i));
end
%%
x = 1:round(length(sortedPrices(1,:))/800):length(sortedPrices(1,:));
xcont = x((x < 157700 & x > 300));
%%
%view(3);
%surf(1:length(sortedPrices(1,:)), co2Prices, sortedPrices);
conts = 15;
%c = surf(x, co2Prices, sortedPrices(:,x),'LineStyle','none'); hold on;

c = imagesc(x, co2Prices, sortedPrices(:,x));
%c = imagesc(x, co2Prices, sortedTypes(:,x));
hold on;
set(gca,'YDir','normal');
cont= contour(xcont, co2Prices, sortedPrices(:,xcont), 12:conts:181, 'k--', 'ShowText', 'on', 'LabelSpacing', 144*2.2);
%z = get(c, 'ZData');
%set(c,'ZData',z-500);
%view(2);
colormap jet;
%h = colorbar;
xlim([0,max(x)]);
xlabel("Hours of the Life Cycle");
ylabel("CO2 Price [€/t]")
h.Label.String = "Electricity Price [€/MWh]";
caxis([0,190]);
col = colorbar;
col.Label.String = "Electricity Price [€/MWh]";
%m = [0 0.4470 0.7410;0.8500 0.3250 0.0980;0.9290 0.6940 0.1250;0.4940 0.1840 0.5560;0.4660 0.6740 0.1880;0.3010 0.7450 0.9330]
%colormap(m);
%colorbar('Ticks',[0,1,2,3,4,5], 'TickLabels',{'No Plant running','Lignite', 'HardCoal', 'Steam turbine', 'Open Cycle', 'Combined Cycle'})
%%
