%% get data
[prices, production, co2Prices] = getPrices();


sortedPrices = zeros(size(prices));
for i = 1:length(prices(:,1))
    sortedPrices(i,:) = sort(prices(i,:), 'descend');
end
%% plot it
x = 1:round(length(sortedPrices(1,:))/150):length(sortedPrices(1,:));
x2 = 1:length(sortedPrices(1,:))/10:length(sortedPrices(1,:));
x3 = 1:length(sortedPrices(1,:))/10:length(sortedPrices(1,:));
%%
%surf(1:length(sortedPrices(1,:)), co2Prices, sortedPrices);
m = mesh(x, co2Prices, sortedPrices(:,x), 'LineStyle','-');
hold on;
x2 = 1:10:length(co2Prices);
colormap jet;
%mesh(x, co2Prices(x2), sortedPrices(x2,x) - 2);
view(45, 10);

xlim([0,max(x)]);
zlim([0,250]);
caxis([0,190]);

xlabel("Hours of the Life Cycle");
ylabel("CO2 Price [€/t]")
zlabel("Electricity Price [€/MWh]")

%%
