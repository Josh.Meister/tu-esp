% we should use checkcode to ensure code quality
% add 'linter' to your local runscript so its executed everytime you test something
% at least lint before commiting

% add new files here
files = {
    'batteries/calculateChargeCycles.m'
    'meritOrder/electricityPrice.m'
    'agora/makeLifecycleCurves.m'
    'agora/getConsumptionData.m'
    'agora/getGenerationData.m'
    'partload/partload.m'
    'linter.m'
    'meritOrder/getMeritOrder.m'
};

checkcode(files)
