function output = arrhenius(y0, deltaT)
    output = y0 .* 2.^(-deltaT/10);
    return
end
