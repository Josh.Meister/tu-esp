function energy = batteryEnergy(deltaC_cyc, age, systemProps)

    if age == 0
        deltaC_cal = 1;
    else
        deltaC_cal = -0.05837*(arrhenius(age/24/365,15))^0.5053 + 1.004;
    end
    %rCCycles = -7.761e-5 * eqFullCycles ^ 0.8405 + 0.9968;
   %woehler = @(x) 1.819e-5*x^4-2.685e-5*x^3+2.818e-5*x^2-1.543e-6*x+9.755e-8;
    energy = systemProps.energy * (deltaC_cal - deltaC_cyc);
    return
end
