function totalInvestmentCosts = calculateInvestmentCosts(systemProps, scenario)

    energy = systemProps.energy;
    power = systemProps.power;

    costsPerEnergy = energy*scenario.costsPerEnergy;
    costsPerPower = power*scenario.costsPerPower;

    totalInvestmentCosts = costsPerEnergy + costsPerPower;

end
