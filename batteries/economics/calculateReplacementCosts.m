function replacementCosts = calculateReplacementCosts(systemProps, scenario, chargeCycles)

    replacementCosts = sum(chargeCycles(7,:)) * systemProps.energy * scenario.costsPerEnergy;

end
