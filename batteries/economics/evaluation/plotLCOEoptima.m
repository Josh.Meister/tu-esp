[prices, generations, co2price] = getPrices();

minLcoes = [];
bestSizes = [];
co2prices = [];

samples = [1, 10, 20, 50, 70];

for i = 1:length(samples)

    disp('---')
    disp(strcat(num2str(i), ' from .', num2str(length(samples))));

    [x, y] = optimizeLCOE(prices(samples(i),:));

    minLcoes = [minLcoes; x];
    bestSizes = [bestSizes; y];

    co2prices = [co2prices co2price(samples(i))];

end

scens = scenarios();

plot(co2prices, bestSizes);

legend(scens.name)
