function [sellOvers, sizes, lceos] = calcLCOEGrid(sellOverRange, sizeRatioRange, price)
    buyUnder = 0;

    sellOvers = sellOverRange;
    sizes = sizeRatioRange;

    x = 1:length(sellOvers);
    y = 1:length(sizes);
    scens = scenarios();
    lcoes = zeros(length(x), length(y), height(scens));
    sizeLength = length(sizes);

    battery.energy = 1;
    battery.roundTrip = 0.85;
    for i = 2:length(sellOvers)

        for j = 1:sizeLength
            battery.power = battery.energy*sizes(j);

            cycles = calculateChargeCycles(price, battery, buyUnder, sellOvers(i));
            lifeCycleCosts = calculateLifeCycleCosts(battery, scens, cycles);
            lcoes(i,j,:) = calculateLCOE(cycles, lifeCycleCosts);
        end
    end
end
