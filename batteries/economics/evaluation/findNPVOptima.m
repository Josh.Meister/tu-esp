dat = load("data/noReplNPVGrid.mat")

npvs = dat.npvs;
sizes = dat.sizes;
sellOvers = dat.sellOvers;

opts.bestSellOvers = [];
opts.bestSizes = [];
opts.bestNPVs = [];

for i = 1:100

    bestNPVs = [];
    bestSizes = [];
    bestSellOvers = [];

    for j = 1:4

        thisNPVGrid = npvs(i,:,:,j);

        [values, xpos] = max(thisNPVGrid);
        [value, ypos] = max(values);

        bestNPVs = [bestNPVs value];
        bestSellOvers = [bestSellOvers sellOvers(xpos(ypos))];
        bestSizes = [bestSizes sizes(ypos)];

    end

    opts.bestNPVs = [opts.bestNPVs; bestNPVs];
    opts.bestSizes = [opts.bestSizes; bestSizes];
    opts.bestSellOvers = [opts.bestSellOvers; bestSellOvers];

end
