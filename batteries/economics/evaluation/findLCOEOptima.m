dat = load("data/noReplLCOEGrid.mat")

lcoes = dat.lcoes;
sizes = dat.sizes;
sellOvers = dat.sellOvers;

opts.bestSellOvers = [];
opts.bestSizes = [];
opts.bestLCOEs = [];

for i = 1:100

    bestLCOEs = [];
    bestSizes = [];
    bestSellOvers = [];

    for j = 1:4

        thisLCOEGrid = lcoes(i,:,:,j);

        [values, xpos] = min(thisLCOEGrid);
        [value, ypos] = min(values);

        bestLCOEs = [bestLCOEs value];
        bestSellOvers = [bestSellOvers sellOvers(xpos(ypos))];
        bestSizes = [bestSizes sizes(ypos)];

    end

    opts.bestLCOEs = [opts.bestLCOEs; bestLCOEs];
    opts.bestSizes = [opts.bestSizes; bestSizes];
    opts.bestSellOvers = [opts.bestSellOvers; bestSellOvers];

end
