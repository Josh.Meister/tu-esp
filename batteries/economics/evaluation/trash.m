plot(co2prices(1:end), opts.bestLCOEs - opts2.bestLCOEs);
legend(scens.name, 'Location', 'best');
xlabel("CO_{2} Price [€/t]");
ylabel("\Delta Optimized LCOE [€/MWh]")
%ylabel("\Delta Optimized NPV_{Spec}/\Delta CO_{2} Price [t/MWh]");
