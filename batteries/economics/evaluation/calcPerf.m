[prices, generations, co2price] = getPrices();

buyUnder = 0;
sellOver = 1:20;

lcoes = [];
npvs = [];
co2prices = [];

sizes = 1:1:20;

%for i = 1:1:length(prices(:,1))
for i = 1:length(sellOver)

    price = prices(1,:);
    co2prices = [co2prices co2price(i)];

    for e = sizes
        sysprops.power = e*10/2;
        sysprops.energy = 100;
        sysprops.roundTrip = 0.9;
        investmentcosts = calculateInvestmentCosts(sysprops, scenarios());
        cycles = calculateChargeCycles(price, sysprops, buyUnder, i*5);
        npvs(i,e,:) = calculateNPV(cycles, investmentcosts, 0);
        lcoes(i,e,:) = calculateLCOE(cycles, investmentcosts);
    end

    disp(i/length(sellOver));

end
