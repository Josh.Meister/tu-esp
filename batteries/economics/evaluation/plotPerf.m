hold on;
for c = 1:length(lcoes(1,1,:))
    mesh(sizes, sellOver, npvs(:,:,c));
end
view(3)
%plot(co2prices, eout)
set(gca, 'ZScale', 'log')
zlabel("NPV [€]");
ylabel("Sell Over [€/MWh]")
xlabel("Power to Energy ratio [1/10]")
