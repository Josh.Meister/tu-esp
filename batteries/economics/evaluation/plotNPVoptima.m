[prices, generations, co2price] = getPrices();

bestNPVs = [];
bestSizes = [];
bestSellOvers = [];
co2prices = [];

samples = 1:10:100;
%samples = [1];

for i = 1:length(samples)

    disp('---')
    disp(strcat(num2str(i), ' from .', num2str(length(samples))));

    [x, y, z] = optimizeNPV(prices(samples(i),:));

    bestNPVs = [bestNPVs; x];
    bestSizes = [bestSizes; z];
    bestSellOvers = [bestSellOvers; y];

    co2prices = [co2prices co2price(samples(i))];

end

scens = scenarios();

plotOptimaMap(co2prices, bestSizes, bestSellOvers);
