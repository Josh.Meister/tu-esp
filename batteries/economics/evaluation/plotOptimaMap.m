function [] = plotOptimaMap(co2prices, bestSizes, bestSellOvers)

    hold on;

    markers = ['>','d','h','<'];

    plots = [];

    for i = 1:length(markers)
        plots(i) = scatter(co2prices(1:2:end), bestSizes(1:2:end,i), [], bestSellOvers(1:2:end,i), markers(i), 'filled');
    end

    scens = scenarios();
    legend(plots, scens.name, 'Location', 'Southeast');

    xlabel('CO_{2} Price [€/t]');
    ylabel('P/S [W/Wh]');

    colormap jet;
    c = colorbar;
    c.Label.String = "SOP [€/MWh]";

end
