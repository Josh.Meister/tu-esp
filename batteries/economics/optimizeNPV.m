function [optNPV, optSellOver, optSizeRatio] = optimizeNPV(price)

    disp('optimizing npv...');

    buyUnder = 0;

    sellOverRes = 10;
    sizeRes = 10;

    sellOvers = linspace(1, 150, sellOverRes);
    sizes = linspace(0, 1, sizeRes);

    npvs = [];

    x = 1;
    y = 1;

    for sellOver = sellOvers

        disp(strcat(num2str(100* x / sellOverRes),' %'));

        y = 1;

        for size = sizes

            sysprops.power = size;
            sysprops.energy = 1;
            sysprops.roundTrip = 0.9;

            cycles = calculateChargeCycles(price, sysprops, buyUnder, sellOver);

            lifeCycleCosts = calculateLifeCycleCosts(sysprops, scenarios(), cycles);

            npvs(x,y,:) = calculateNPV(cycles, lifeCycleCosts);

            y = y + 1;

        end

        x = x + 1;

    end

    optNPV = [];
    optSellOver = [];
    optSizeRatio = [];

    %hold on;
    %view(3);

    for i = 1:length(npvs(1,1,:))

        [values, xpos] = max(npvs(:,:,i));
        [value, ypos] = max(values);

        % mesh(sizes, sellOvers, npvs(:,:,i));

        optNPV = [optNPV value];
        optSellOver = [optSellOver sellOvers((xpos(ypos)))];
        optSizeRatio = [optSizeRatio sizes(ypos)];

    end

end
