function lcoe = calculateLCOE(chargeCycles, lifeCycleCosts)

    totalEnergyDischarged = 0;
    chargingCosts = 0;

    totalEnergyDischarged = abs(sum(chargeCycles(2, chargeCycles(2,:) < 0)));

    % > for only costs, < for also revenues
    chargingCosts = sum(chargeCycles(4, chargeCycles(4,:) > 0));

    lcoe = (lifeCycleCosts + chargingCosts) / totalEnergyDischarged;

end
