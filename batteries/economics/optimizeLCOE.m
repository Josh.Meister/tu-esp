function [optLCOE, optSizeRatio] = optimizeLCOE(price)

    disp('optimizing lcoe...');

    buyUnder = 0;

    sizeRes = 20;
    sizes = linspace(0, 1, sizeRes);

    lcoes = [];

    x = 1;

    for size = sizes

        disp(strcat(num2str(100* x / sizeRes),' %'));

        sysprops.power = size;
        sysprops.energy = 1;
        sysprops.roundTrip = 0.9;

        cycles = calculateChargeCycles(price, sysprops, buyUnder, 0);

        lifeCycleCosts = calculateLifeCycleCosts(systemProps, scenario, cycles)

        lcoes(x,:) = calculateLCOE(cycles, lifeCycleCosts);

        x = x + 1;

    end

    optLCOE = [];
    optSellOver = [];
    optSizeRatio = [];

    %hold on;

    for i = 1:length(lcoes(1,:))

        [value, pos] = min(lcoes(:,i));

        %plot(sizes, lcoes(:,i));

        optLCOE = [optLCOE value];
        optSizeRatio = [optSizeRatio sizes(pos)];

    end

end
