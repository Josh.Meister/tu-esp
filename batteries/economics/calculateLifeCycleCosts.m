function lifeCycleCosts = calculateLifeCycleCosts(systemProps, scenario, chargeCycles)

    lifeCycleCosts = calculateInvestmentCosts(systemProps, scenario) + calculateReplacementCosts(systemProps, scenario, chargeCycles) + calculateMaintenanceCosts(systemProps, scenario, chargeCycles);

end
