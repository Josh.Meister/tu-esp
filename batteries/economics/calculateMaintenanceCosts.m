function maintenanceCosts = calculateMaintenanceCosts(systemProps, scenario, chargeCycles)

    omFixed = scenario.maintenancePerPower * systemProps.power * 20;
    omVariable = scenario.maintenancePerEnergy * abs(sum(chargeCycles(2, chargeCycles(2,:) < 0)));

    maintenanceCosts = omFixed + omVariable;

end
