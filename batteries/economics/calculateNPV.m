function npv = calculateNPV(chargeCycles, lifeCycleCosts)

    npv = - lifeCycleCosts - sum(chargeCycles(4,:));

end
