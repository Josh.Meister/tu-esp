function [chargeCycles, halfCycleHubs] = calculateChargeCycles(electricityPrice, systemProps, buyUnder, sellOver)
    % chargeCycles: (1) batteryElectricityFlow, (2) marketElectricityFlow
    % , (3) stateOfCharge, (4) moneyFlow, (5) storageCapacity

    % req systemprop fields: energy, roundTrip, power
    %woehler = load("batteries/physics/woehlerCurve.mat").woehler;
    %woehler = @(x) 1.819e-5*x^4-2.685e-5*x^3+2.818e-5*x^2-1.543e-6*x+9.755e-8;
    woehler = @(x)-1.467e-05*x^3+3.804e-5*x^2+9.442e-6*x;
    chargeCycles = zeros(4,length(electricityPrice));
    halfCycleHubs = [];
    doReplacements = true;
    if isfield(systemProps, 'replace') && systemProps.replace==false
        doReplacements = false;
    end

    currentStateOfCharge = 0;
    eqFullCycles = 0;
    age = 0;
    replTreshhold = 0.8;
    currRoundTrip = systemProps.roundTrip;
    halfCycleHub = 0;
    deltaC_cyc = 0;

    for i = 1:length(electricityPrice)
        currStorageCapacity = batteryEnergy(deltaC_cyc, age, systemProps);
        %currRoundTrip = batteryEfficiency(eqFullCycles, age, systemProps);
        currChEff = sqrt(currRoundTrip);

        if electricityPrice(i) <= buyUnder % try to buy electricity

            if currStorageCapacity - currentStateOfCharge >= systemProps.power*currChEff
                batteryElectricityFlow = systemProps.power * currChEff;
                marketElectricityFlow = systemProps.power;
            elseif currStorageCapacity > currentStateOfCharge
                batteryElectricityFlow = currStorageCapacity - currentStateOfCharge;
                marketElectricityFlow = batteryElectricityFlow / currChEff;
            else
                batteryElectricityFlow = 0;
                marketElectricityFlow = 0;
            end

        elseif electricityPrice(i) <= sellOver % do nothing if possible
            batteryElectricityFlow = 0;
            marketElectricityFlow = 0;

        else % try to sell electricity

            if currentStateOfCharge >= systemProps.power / currChEff
                batteryElectricityFlow = - systemProps.power / currChEff;
                marketElectricityFlow = - systemProps.power;

            elseif currentStateOfCharge > 0
                batteryElectricityFlow = -currentStateOfCharge;
                marketElectricityFlow = -currentStateOfCharge * currChEff;

            else
                batteryElectricityFlow = 0;
                marketElectricityFlow = 0;
            end

        end

        stateOfCharge = currentStateOfCharge + batteryElectricityFlow;
        moneyFlow = marketElectricityFlow * electricityPrice(i);
        chargeCycles(1, i) = batteryElectricityFlow;
        chargeCycles(2, i) = marketElectricityFlow;
        chargeCycles(3, i) = stateOfCharge;
        chargeCycles(4, i) = moneyFlow;
        chargeCycles(5, i) = currStorageCapacity/systemProps.energy;
        chargeCycles(6, i) = eqFullCycles;
        chargeCycles(7, i) = 0;
        
        if sign(halfCycleHub)==sign(batteryElectricityFlow) || halfCycleHub == 0
            halfCycleHub = halfCycleHub + batteryElectricityFlow/currStorageCapacity;
        else
            deltaC_cyc = deltaC_cyc + 1/2 * woehler(abs(halfCycleHub));
            halfCycleHubs = [halfCycleHubs halfCycleHub];
            halfCycleHub = batteryElectricityFlow/currStorageCapacity;
        end

        currentStateOfCharge = chargeCycles(3, i);
        eqFullCycles = eqFullCycles + abs(batteryElectricityFlow)/currStorageCapacity;
        age = age + 1;

        if doReplacements
            if currStorageCapacity < replTreshhold * systemProps.energy
                age = 0;
                eqFullCycles = 0;
                deltaC_cyc = 0;
                halfCycleHub = 0;
                chargeCycles(7, i) = 1;
            end
        end
    end
