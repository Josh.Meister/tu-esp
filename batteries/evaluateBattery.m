function [lcoe, npv, chargeCycles] = evaluateBattery(systemProps, co2tax, discountRate, buyUnder, sellOver)
    % perform an evaluation of the given system setup for a given co2tax

    electricityPrice = load('testPrice.mat').price();

    chargeCycles = calculateChargeCycles(electricityPrice, systemProps, buyUnder, sellOver);

    investmentCosts = calculateInvestmentCosts(systemProps);

    lcoe = calculateLCOE(chargeCycles, investmentCosts);

    npv = calculateNPV(chargeCycles, investmentCosts, discountRate);

end
