steamDat = load('partload/steam.csv');
openCycleDat = load('partload/openCycle.csv');
combinedCycleDat = load('partload/combinedCycle.csv');
% combustionEngineDat = load('partload/combustionEngine.csv');

steamFit = load('partload/steam.mat').steamFit;
openCycleFit = load('partload/openCycle.mat').openCycleFit;
combinedCycleFit = load('partload/combinedCycle.mat').combinedCycleFit;
% combustionFit = load('partload/combustionFit.mat').combustionFit;

hold on;

x = 0:0.01:1;

plot(x, steamFit(x),'r');
plot(x, openCycleFit(x),'b');
plot(x, combinedCycleFit(x),'k');

scatter(steamDat(:,1),steamDat(:,2),'o','r','filled');
scatter(openCycleDat(:,1),openCycleDat(:,2),'d','b','filled');
scatter(combinedCycleDat(:,1),combinedCycleDat(:,2),'s','k','filled');

%scatter(combustionEngineDat(:,1),combustionEngineDat(:,2),'d','MarkerColor':'#7E2F8E');
%plot(x, combustionFit(x),'Color','#7E2F8E');

xline(0.15, '-.b','OCGT','LabelVerticalAlignment','bottom');
xline(0.2, '-.k','CCGT','LabelVerticalAlignment','bottom');
xline(0.3, '-.r', 'Hard Coal','LabelVerticalAlignment','bottom');
xline(0.5, '-.r', 'Lignite Coal','LabelVerticalAlignment','bottom');
% xline(0.1, ':', 'CE','LabelVerticalAlignment','bottom','Color','#7E2F8E');

ylim([0,1.01]);

legend("ST", "OCGT", "CCGT", 'Location', 'southeast')

xlabel('Load Factor [1]');
ylabel('Relative Efficiency [1]');

% saveFig(1);
