function partloadFits = loadfits()
    partloadFits.steamFit = load('steam.mat').steamFit;
    partloadFits.openCycleFit = load('openCycle.mat').openCycleFit;
    partloadFits.combinedCycleFit = load('combinedCycle.mat').combinedCycleFit;
end
