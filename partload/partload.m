function [partloadEfficiency] = partload(plant, demandedLoad, partloadFits)
    % return the plants effiency in partload

    switch plant(4)
    case 6 % open cycle gas turbine
        fit = partloadFits.openCycleFit;
        minLoadFactor = 0.15;
    case 7 % combined cycle gas turbine
        fit = partloadFits.combinedCycleFit;
        minLoadFactor = 0.2;
    case 5
        fit = partloadFits.steamFit;
        if plant(5) == 1 % lignite
            minLoadFactor = 0.5;
        elseif plant(5) == 0 % hardcoal
            minLoadFactor = 0.3;
        else % combined cycle "gas" turbine
            minLoadFactor = 0.2;
        end
    otherwise
        disp('plant type unknown')
    end

    if demandedLoad < minLoadFactor * plant(2)
        partloadEfficiency = 0;
        return;
    end

    partloadEfficiency = fit(demandedLoad / plant(2)) * plant(3);
end
