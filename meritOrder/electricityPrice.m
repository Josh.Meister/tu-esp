function [price, supply, plantTypes] = electricityPrice(demand, co2tax)
    % calculates the electricity price for a given demand

    partloadFits = loadfits();

    price = zeros(1, length(demand));
    supply = zeros(1, length(demand));
    plantTypes = zeros(1, length(demand));

    meritOrder = getMeritOrder(co2tax);

    for i = 1:length(demand)

        %if mod(100,(length(demand)/i)) == 0
        %    disp(strcat(num2str(round(i/length(demand) * 100))," %"));
        %end

        supplyInterval = 0;
        demandInterval = demand(i);

        plantNum = 0;
        marginalPlant = NaN;

        while supplyInterval < demandInterval

            plantNum = plantNum + 1;

            try
                if supplyInterval + meritOrder(plantNum, 2) <= demandInterval
                    supplyInterval = supplyInterval + meritOrder(plantNum,2);
                    marginalPlant = meritOrder(plantNum,:);
                else
                    loadGap = demandInterval - supplyInterval;
                    avaiablePlants = meritOrder(plantNum:length(meritOrder),:);
                    partloadPlant = findPartloadPlant(avaiablePlants, loadGap, partloadFits);

                    if ~isnan(partloadPlant)
                        supplyInterval = supplyInterval + loadGap;
                        marginalPlant = partloadPlant;
                    else
                        break;
                    end
                end
            catch error
                disp('Unable to match demand');
                rethrow(error);
                break;
            end

        end

        try
            price(i) = marginalPlant(7) + marginalPlant(8);
            supply(i) = supplyInterval;
            plantTypes(i) = getPlantType(marginalPlant);
        catch error
            continue;
        end

    end
end
