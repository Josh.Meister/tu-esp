plantData = [];
plantNames = string([]);
plantTypes = load("plantdata.mat").plantTypes;

for i=1:320
    if i <= 24
        type = 0;
        capacity = 300 + rand() * 800;
        marginalCosts = 20 + rand()*60;
        specificCo2Em = 10 + rand() * 30;
    elseif i <= 43
        type = 1;
        capacity = 200 + 900 * rand();
        marginalCosts = 30 + rand() * 70;
        specificCo2Em = 15 + rand() * 30;
    elseif i <= 180
        type = 2;
        capacity = 80 + 400 * rand();
        marginalCosts = 60 + rand() * 30;
        specificCo2Em = 5 + rand() * 20;
    else
        type = 3;
        capacity = 60 + 300 * rand();
        marginalCosts = 80 + rand()*30;
        specificCo2Em = 3 + rand() * 10;
    end
    plantData(i,:) = [i, type, capacity, marginalCosts, specificCo2Em, rand()*3];
    plantNames(i,:) = string([int2str(i), "Plant " + i + ", type " + type]);
end
plantData
plantNames
plantTypes
