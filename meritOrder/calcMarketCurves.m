function calcMarketCurves(loadGap, co2PricesArray)
    append = false; % if set to true just appends the file, doesn't check overwriting
    redundantGap = true; % if set to true give only the non redundant Gap part, it gets repeated
    filename = 'data/marketCurves2.mat';
    if isfile(filename) && ~append
        disp("File " + filename + " exists. Aborting to prevent overwriting.")
        return
    end

    count = 1;
    totalTime = 0;
    for i=co2PricesArray
        tic;
        name = "mC_" + string(i);
        [marketCurves.price marketCurves.production marketCurves.marginalType] = electricityPrice(loadGap, i);
        marketCurves.co2Price = i;
        if redundantGap
            marketCurves.price = [marketCurves.price, marketCurves.price];
            marketCurves.production = [marketCurves.production, marketCurves.production];
            marketCurves.marginalType = [marketCurves.marginalType, marketCurves.marginalType];
        data.(name) = marketCurves;

        if count > 2 || append
            save(filename, '-struct', 'data', '-append');
        else
            save(filename, '-struct', 'data');
        end

        stoppedTime = toc;
        totalTime = totalTime + stoppedTime;

        disp(string(round(count/length(co2PricesArray)*100,1)) + "%. ------ Current Co2Tax: " + string(i));
        eta = stoppedTime * (length(co2PricesArray)-count)/60;
        if eta > 60
            remTimeStr = string(round(eta/60,2)) + "h";
        else
            remTimeStr = string(round(eta,2)) + "min";
        end
        disp("... Elapsed time: " + string(round(totalTime,2)) + ", ETA: " + remTimeStr);
        count = count + 1;
    end
end
