function sortedPlants = getMeritOrder(co2Price)
    % data: IdNumber[1], Capacity[2], Efficiency[3], Technology[4], Fueltype[5], isCHP[6]
    % (spec Costs is calculated here)[7]
    data = load("plantdata.mat");
    technicalData = data.plantData;
    typeSpecCo2Emission = [0.339, 0.404, 0.202]; % given by project description
    typeFuelCost = [13.4, 1.8, 32.2]; % given by project description
    opexVar = [5, 3, 4];

    for i = 1:length(technicalData)
        % technicalData(i, 4) = technicalData(i, 4) + co2Price * technicalData(i, 5);
        fuelIndex = technicalData(i, 5) + 1;
        technicalData(i, 7) = (typeFuelCost(fuelIndex) + co2Price*typeSpecCo2Emission(fuelIndex))/technicalData(i,3);
        opexIndex = technicalData(i, 4) - 4;
        technicalData(i, 8) = opexVar(opexIndex);
    end

    %technicalData(:,9) = technicalData(:,7) + technicalData(:,8);
    %sortedPlants = sortrows(technicalData, 9);
    [o,I] = sort(technicalData(:,7) + technicalData(:,8));
    sortedPlants = technicalData(I,:);
    return
end
