function [cheapestPlant] = findPartloadPlant(avaiablePlants, loadGap, partloadFits)
    % find the plant that can generate the load gap by going partload at the lowest price

    currentBid = Inf;
    cheapestPlant = NaN;

    for plant = avaiablePlants'

        if plant(7) > currentBid
            break;
        end

        if plant(2) <= loadGap
            continue;
        end

        partloadEfficiency = partload(plant,loadGap,partloadFits);
        thisPlantBid = plant(7)/(partloadEfficiency/plant(3));

        if thisPlantBid < currentBid

            currentBid = thisPlantBid;
            cheapestPlant = plant;
            cheapestPlant(7) = currentBid;

        end

    end

end
