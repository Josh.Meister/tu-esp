function [] = saveFig(yScale, linScale)
    set(gcf, 'color', 'white');
    set(gcf, 'Position', [100 100 100+400*sqrt(2)*linScale 100+400*yScale*linScale]);
    export_fig data/plots/figure -pdf -transparent;
end
