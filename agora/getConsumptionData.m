function [data] = getConsumptionData(scenarioName, weatherScenarios)
    % import the consumption data for a specific case and weather scenario from the xlsx file
    try
        sheetName = strcat(scenarioName, '_DE_consumption');
        allConsumptionData = readtable('consumption.xlsx', 'Sheet', sheetName);
    catch ME
        disp("Error encountered in scenario name, using default scenario")
        sheetName = strcat("FRhigh,DElow-50%RES", '_DE_consumpt');
        allConsumptionData = readtable('consumption.xlsx', 'Sheet', sheetName);
    end

    data = [];

    for weatherScenario = weatherScenarios
        collumnName = strcat('TestCase', num2str(weatherScenario), 'PowerDemandElectricity');
        data = [data; allConsumptionData.(collumnName)];
    end

end
