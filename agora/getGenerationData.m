function [data] = getGenerationData(scenarioName, weatherScenarios, source)
    % import the generation data for a specific case, weather scenario and source from the xlsx file

    sheetName = strcat(scenarioName, '_DE_production');

    allGenerationData = readtable('generation.xlsx', 'Sheet', sheetName);

    suffixes = [
        "01_OtherThermalFleetElectricity"
        "02_OtherRenewableFleetElectricity"
        "05_WindOnshoreFleetElectricity"
        "06_WindOffshoreFleetElectricity"
        "07_SolarFleetElectricity"
        "HydroElectricity"
    ];

    data = [];

    switch source
      case 'otherThermal'
          sourceSuffix = suffixes(1);
      case 'otherRenewable'
          sourceSuffix = suffixes(2);
      case 'windOnshore'
          sourceSuffix = suffixes(3);
      case 'windOffshore'
          sourceSuffix = suffixes(4);
      case 'solar'
          sourceSuffix = suffixes(5);
      case 'hydro'
          sourceSuffix = suffixes(6);
      case 'all'
          for weatherScenario = weatherScenarios
              collumnName = strcat('TestCase', num2str(weatherScenario), suffixes);
              tempData = allGenerationData.(collumnName(1));
              for i = 2:6
                  tempData = tempData + allGenerationData.(collumnName(i));
              end
              data = [data; tempData];
          end
          return;
      otherwise
          disp('unknown source given');
    end

    for weatherScenario = weatherScenarios
        collumnName = strcat('TestCase', num2str(weatherScenario), sourceSuffix);
        data = [data; allGenerationData.(collumnName)];
    end

end
