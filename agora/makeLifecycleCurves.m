% script to generate lifecylce curves of demand and generation.

scenarioName = 'FRlow,DEmed';
weatherScenarios = [0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9];

disp('making lifecycleGenerationAll')
lifecycleGenerationAll = getGenerationData(scenarioName,weatherScenarios,'all');

disp('making lifecycleConsumption')
lifeCycleConsumption = getConsumptionData(scenarioName,weatherScenarios);

disp('making lifecycleGap');
limit = length(weatherScenarios) * 8760;
lifeCycleGap = zeros(1,limit);
for i=1:limit
    gap = lifeCycleConsumption(i) - lifecycleGenerationAll(i);
    if gap >= 0
        lifeCycleGap(i) = gap;
    else
        lifeCycleGap(i) = 0;
    end
end

disp('--- done! ---')
