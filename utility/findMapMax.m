function [maximum, ind1, ind2] = findMapMax(map)
    [maxima, indexes2] = max(map');
    [maximum, ind1] = max(maxima);
    ind2 = indexes2(ind1);
end
