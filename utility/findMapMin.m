function [minimum, ind1, ind2] = findMapMax(map)
    [minima, indexes2] = min(map');
    [minimum, ind1] = min(minima);
    ind2 = indexes2(ind1);
end
